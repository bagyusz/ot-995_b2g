# Custom kernel header
TARGET_SPECIFIC_HEADER_PATH := device/alcatel/cocktail/include

TARGET_BOOTLOADER_BOARD_NAME := cocktail
TARGET_BOARD_PLATFORM := msm7x30
TARGET_BOARD_PLATFORM_GPU := qcom-adreno200
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true
TARGET_NO_INITLOGO := true

TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
ARCH_ARM_HAVE_TLS_REGISTER := true

# Screens dimension
TARGET_SCREEN_HEIGHT := 800
TARGET_SCREEN_WIDTH := 480

# Kernel/bootimg
BOARD_KERNEL_CMDLINE := console=ttyDCC0 androidboot.hardware=cocktail
BOARD_KERNEL_BASE := 0x00200000
BOARD_KERNEL_PAGESIZE := 4096
BOARD_FORCE_RAMDISK_ADDRESS := 0x01300000
TARGET_PREBUILT_KERNEL := device/alcatel/cocktail/kernel


# cat /proc/partitions
 
#major     minor  #blocks name

# 179        0    1912832 mmcblk0
# 179        1      51200 mmcblk0p1
# 179        2        500 mmcblk0p2
# 179        3       1500 mmcblk0p3
# 179        4          1 mmcblk0p4
# 179        5       1000 mmcblk0p5
# 179        6       2000 mmcblk0p6
# 179        7       3072 mmcblk0p7
# 179        8       3072 mmcblk0p8
# 179        9       5120 mmcblk0p9 ## boot
# 179       10       7000 mmcblk0p10
# 179       11       3072 mmcblk0p11
# 179       12       3072 mmcblk0p12
# 179       13       1024 mmcblk0p13
# 179       14     256000 mmcblk0p14 ## system
# 179       15     307200 mmcblk0p15 ## custpack:= system/app
# 179       16    1039360 mmcblk0p16 ## data
# 179       17     204800 mmcblk0p17 ## cache
# 179       18       5120 mmcblk0p18 ## recovery
# 179       19       1024 mmcblk0p19
# 179       20       5359 mmcblk0p20

# Partition sizes
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x0500000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x0550000
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 262144000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 1064304640
BOARD_CACHEIMAGE_PARTITION_SIZE := 209715200
BOARD_FLASH_BLOCK_SIZE := 4096

# Recovery
BOARD_USES_MMCUTILS := true
BOARD_HAS_NO_MISC_PARTITION := true
BOARD_HAS_NO_SELECT_BUTTON := true
TARGET_PROVIDES_INIT := true
TARGET_PROVIDES_INIT_TARGET_RC := true
TARGET_RECOVERY_PIXEL_FORMAT := RGBX_8888
BOARD_UMS_LUNFILE := "/sys/class/android_usb/android0/f_mass_storage/lun0/file"
TARGET_PREBUILT_RECOVERY_KERNEL := device/alcatel/cocktail/kernel_recovery
TARGET_RECOVERY_INITRC := device/alcatel/cocktail/init.recovery.rc
TARGET_RECOVERY_FSTAB := device/alcatel/cocktail/recovery.fstab

# Vold
BOARD_VOLD_MAX_PARTITIONS := 20
BOARD_VOLD_EMMC_SHARES_DEV_MAJOR := true
TARGET_USE_CUSTOM_LUN_FILE_PATH := /sys/class/android_usb/android0/f_mass_storage/lun0/file

# Graphics
BOARD_EGL_CFG := device/alcatel/cocktail/egl.cfg
USE_OPENGL_RENDERER := true

# QCOM Display
BOARD_ADRENO_DECIDE_TEXTURE_TARGET := true
BOARD_USES_ADRENO_200 := true
TARGET_NO_HW_VSYNC := true
#TARGET_USES_SF_BYPASS := false
#TARGET_HAVE_BYPASS := false
#TARGET_USES_OVERLAY := true
#TARGET_GRALLOC_USES_ASHMEM := true
#TARGET_USES_GENLOCK := true
TARGET_USES_C2D_COMPOSITION := true
TARGET_QCOM_HDMI_OUT := true
TARGET_QCOM_HDMI_RESOLUTION_AUTO := true

# Camera
COMMON_GLOBAL_CFLAGS += -DICS_CAMERA_BLOB
BOARD_NEEDS_MEMORYHEAPPMEM := true
TARGET_DISABLE_ARM_PIE := true
USE_CAMERA_STUB := true

# FM Radio
BOARD_HAVE_FM_RADIO := true
BOARD_FM_DEVICE := bcm4330
BOARD_GLOBAL_CFLAGS += -DHAVE_FM_RADIO

# Webkit
USE_OPENGL_RENDERER := true
ENABLE_WEBGL := true
TARGET_FORCE_CPU_UPLOAD := true

# QCOM
COMMON_GLOBAL_CFLAGS += -DREFRESH_RATE=60 -DQCOM_HARDWARE
BOARD_USES_QCOM_GPS := true
BOARD_USES_QCOM_LIBRPC := true
BOARD_USES_QCOM_LIBS := true
BOARD_USES_QCOM_HARDWARE := true
BOARD_VENDOR_QCOM_AMSS_VERSION := 6225
BOARD_VENDOR_QCOM_GPS_LOC_API_HARDWARE := cocktail
BOARD_VENDOR_QCOM_GPS_LOC_API_AMSS_VERSION := 50000

# Input
BOARD_USE_LEGACY_TOUCHSCREEN := true

# radio interface
BOARD_MOBILEDATA_INTERFACE_NAME = "rmnet0"

# WiFi
WIFI_BAND                   := 802_11_ABGN
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd
BOARD_WLAN_DEVICE           := bcmdhd
BOARD_WLAN_DEVICE_REV       := bcm4330_b2
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/bcmdhd/parameters/firmware_path"
WIFI_DRIVER_FW_PATH_STA     := "/system/etc/wifi/firmware/fw_bcmdhd.bin"
WIFI_DRIVER_FW_PATH_AP      := "/system/etc/wifi/firmware/fw_bcmdhd_apsta.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/system/etc/wifi/firmware/fw_bcmdhd_p2p.bin"
BOARD_LEGACY_NL80211_STA_EVENTS := true

# Bluetooth
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true

# Boot animation speedup
TARGET_BOOTANIMATION_PRELOAD := true
TARGET_BOOTANIMATION_TEXTURE_CACHE := true
