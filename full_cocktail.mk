$(call inherit-product, $(LOCAL_PATH)/cocktail.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_eu_supl.mk)

# Set those variables here to overwrite the inherited values.
PRODUCT_NAME := full_cocktail
PRODUCT_DEVICE := cocktail
PRODUCT_BRAND := Alcatel
PRODUCT_MANUFACTURER := Alcatel
PRODUCT_MODEL := OT-995
