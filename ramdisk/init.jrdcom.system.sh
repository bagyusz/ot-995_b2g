#!/system/bin/sh

#
# enable/disable diag interface by ro.kernel.diag.enable 
#
diag_enable=`getprop ro.kernel.diag.enable`
case "$diag_enable" in
    "true")
	echo 1 > /sys/module/diagchar/parameters/enable
	;;

    "false")
	echo 0 > /sys/module/diagchar/parameters/enable
	;;

esac

exit 0