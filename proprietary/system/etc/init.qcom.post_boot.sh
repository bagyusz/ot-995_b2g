#!/system/bin/sh
#!/xbin/sh

# cocktail BT mac reload by bagyusz
FLAG="/system/etc/bluetooth/bdaddr.txt"
if [ ! -f $FLAG ]; then
    grep -sh "^macaddr" /system/etc/wifi/bcmdhd.cal \
         /system/etc/wifi/bcmdhd.cal /system/etc/bluetooth/wifi_mac.txt | head -n1 > /system/etc/bluetooth/wifi_mac.txt
    cat /system/etc/bluetooth/wifi_mac.txt | cut -d= -f2 >> /system/etc/bluetooth/bdaddr.txt
	rm /system/etc/bluetooth/wifi_mac.txt

    chmod 777 /system/etc/bluetooth/bdaddr.txt
	
    setprop ro.bt.bdaddr_path /system/etc/bluetooth/bdaddr.txt

   #the next line creates an empty file so it won't run the next boot
   touch $FLAG
else
   echo "Do nothing"
fi

# power collapse
echo 1 > /sys/module/pm2/parameters/idle_sleep_mode

