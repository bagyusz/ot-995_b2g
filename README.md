Device configuration to build [Firefox OS for Alcatel OT-995]

How to build:
-------------

[Initialize B2G build environment:](https://developer.mozilla.org/en-US/docs/Mozilla/Firefox_OS/Preparing_for_your_first_B2G_build)

    git clone https://bitbucket.org/bagyusz/B2G.git
    cd B2G
    ./config.sh cocktail

Build:

    rm -r frameworks/base/fmradio
    ./build.sh


-----------------------------------------------------------------------------------------------------------------
Error:

frameworks/base/include/utils/KeyedVector.h:193:31: note: declarations in dependent base ‘android::KeyedVector<android::String8, android::sp<AaptDir> >’ are not found by unqualified lookup
frameworks/base/include/utils/KeyedVector.h:193:31: note: use ‘this->indexOfKey’ instead
make: *** [out/host/linux-x86/obj/EXECUTABLES/aapt_intermediates/AaptAssets.o] Error 1

Fix:
vi frameworks/base/tools/aapt/Android.mk

Add '-fpermissive' to line 31:
LOCAL_CFLAGS += -Wno-format-y2k -fpermissive
-----------------------------------------------------------------------------------------------------------------
Error:

frameworks/base/include/utils/KeyedVector.h:193:31: error: ‘indexOfKey’ was not declared in this scope, and no declarations were found by argument-dependent lookup at the point of instantiation [-fpermissive]
frameworks/base/include/utils/KeyedVector.h:193:31: note: declarations in dependent base ‘android::KeyedVector<android::String8, android::wp<android::AssetManager::SharedZip> >’ are not found by unqualified lookup

frameworks/base/include/utils/KeyedVector.h:193:31: note: use ‘this->indexOfKey’ instead
make: *** [out/host/linux-x86/obj/STATIC_LIBRARIES/libutils_intermediates/AssetManager.o] Error 1

Fix:
vi frameworks/base/libs/utils/Android.mk

Add '-fpermissive' to line 64:

LOCAL_CFLAGS += -DLIBUTILS_NATIVE=1 $(TOOL_CFLAGS) -fpermissive

-----------------------------------------------------------------------------------------------------------------
Error

B2G/gecko/client.mk:290: *** Could not find autoconf 2.13.  Stop. 
make[2]: *** [/home/luo/B2G/gecko/objdir-prof-gonk/Makefile] Error 2
make[1]: *** [build] Error 2 
make: *** [gecko] Error 2 

Fix

https://developer.mozilla.org/en-US/docs/Developer_Guide/Build_Instructions/Linux_Prerequisites
